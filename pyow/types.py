from typing import NewType, Any

Predicate = NewType('Predicate', Any)
